const { execFileSync, execSync, spawnSync } = require("child_process");
const {
  cpSync,
  mkdirSync,
  mkdtempSync,
  readFileSync,
  rmSync,
  writeFileSync,
} = require("fs");
const { basename } = require("path");
const { randomUUID } = require("crypto");
const {
  afterAll,
  beforeAll,
  beforeEach,
  describe,
  expect,
  test,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  xdescribe,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  xtest,
} = require("@jest/globals");

const VERSION_FILENAME = "version.json";
const expectTimestamp = function (a) {
  return {
    toBeBefore: function (b) {
      expect(new Date(a).getTime()).toBeLessThan(new Date(b).getTime());
    },
  };
};

describe("aws_sync integration tests", () => {
  const extra_env = {
    AWS_REGION: "eu-west-2",
    AWS_DEFAULT_REGION: "eu-west-2",
    AWS_ACCESS_KEY_ID: "test",
    AWS_SECRET_ACCESS_KEY: "test",
    AWS_ENDPOINT_URL: process.env.AWS_ENDPOINT_URL || "http://localhost:4566/",
    LOG_LEVEL: "info",
  };
  const env = { ...process.env, ...extra_env };
  let workdir;
  let testdir;
  let testdata_dir;
  let upload_dir;
  let bucket;

  const aws = function (args, options = {}) {
    return execSync(
      `aws --endpoint-url="${extra_env.AWS_ENDPOINT_URL}" --region "${extra_env.AWS_REGION}" ${args}`,
      { env, ...options },
    ).toString();
  };
  const aws_sync = function (env_override = {}) {
    return execFileSync("./aws_sync.js", [upload_dir, bucket], {
      env: { ...env, ...env_override },
    }).toString();
  };

  beforeAll(() => {
    workdir = mkdtempSync("aws_sync-");
    testdata_dir = `${workdir}/testdata`;
    mkdirSync(testdata_dir);
    bucket = basename(workdir).toLowerCase().replace("_", "-");
    console.log(`test bucket: ${bucket}`);

    aws(
      `s3api create-bucket --bucket "${bucket}" --create-bucket-configuration=LocationConstraint="${extra_env.AWS_REGION}"`,
    );

    mkdirSync(`${testdata_dir}/foo/bar`, { recursive: true });
    writeFileSync(`${testdata_dir}/index.html`, randomUUID());
    writeFileSync(`${testdata_dir}/pretty.css`, randomUUID());
    writeFileSync(`${testdata_dir}/foo/some_file.txt`, randomUUID());
    writeFileSync(`${testdata_dir}/foo/bar/another_file`, randomUUID());
    writeFileSync(
      `${testdata_dir}/${VERSION_FILENAME}`,
      JSON.stringify({
        application_version: "1.0.0",
        last_data_sync: "2000-01-01T00:00:00.000Z",
        data_changed: "2000-01-02T00:00:00.000Z",
      }),
    );
  });

  beforeEach(() => {
    testdir = mkdtempSync(`${workdir}/test-`);
    upload_dir = mkdtempSync(`${testdir}/upload-`);
    aws(`s3 rm --recursive "s3://${bucket}"`);
    aws(`s3 sync . "s3://${bucket}"`, { cwd: testdata_dir });
    cpSync(testdata_dir, upload_dir, { recursive: true });
  });

  afterAll(() => {
    rmSync(workdir, { recursive: true });
  });

  describe("general setup", () => {
    test("system info", () => {
      console.log(
        `process.versions: ${JSON.stringify(process.versions, null, 2)}`,
      );
    });

    test("sync works", () => {
      aws_sync();
    });

    test("version file is JSON", () => {
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );
      JSON.parse(readFileSync(`${testdir}/new_version`));
    });

    test("version file is well-formed", () => {
      const timestamp_re = /^[0-9TZ:.-]+$/;
      const version_re = /^([0-9]+\.[0-9]+\.[0-9]+|unknown)$/;
      const hostname_re = /^([a-z_-]+|unknown)$/;

      writeFileSync(`${upload_dir}/new_file.txt`, randomUUID());
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );
      const version = JSON.parse(readFileSync(`${testdir}/new_version`));

      expect(version.application_version).toMatch(version_re);
      expect(version.last_data_sync).toMatch(timestamp_re);
      expect(version.data_changed).toMatch(timestamp_re);
      expect(version.panelapp_host).toMatch(hostname_re);
    });
  });

  describe("application version", () => {
    test("application_version is updated", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      aws_sync({ APP_VERSION: "1.2.3" });
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      const old_version = JSON.parse(
        readFileSync(`${testdir}/old_version`),
      ).application_version;
      const new_version = JSON.parse(
        readFileSync(`${testdir}/new_version`),
      ).application_version;

      expect(new_version).not.toBe(old_version);
      expect(new_version).toBe("1.2.3");
    });

    test("application_version default is 'unknown'", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      aws_sync({ APP_VERSION: "" });
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      const old_version = JSON.parse(
        readFileSync(`${testdir}/old_version`),
      ).application_version;
      const new_version = JSON.parse(
        readFileSync(`${testdir}/new_version`),
      ).application_version;

      expect(new_version).not.toBe(old_version);
      expect(new_version).toBe("unknown");
    });
  });

  describe("panelapp_host version", () => {
    test("panelapp_host is updated", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      aws_sync({ PANELAPP_HOSTNAME: "panelapp-prod" });
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      const old_version = JSON.parse(
        readFileSync(`${testdir}/old_version`),
      ).panelapp_host;
      const new_version = JSON.parse(
        readFileSync(`${testdir}/new_version`),
      ).panelapp_host;

      expect(new_version).not.toBe(old_version);
      expect(new_version).toBe("panelapp-prod");
    });

    test("panelapp_host default is 'unknown'", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      aws_sync({ PANELAPP_HOSTNAME: "" });
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      const old_version = JSON.parse(
        readFileSync(`${testdir}/old_version`),
      ).panelapp_host;
      const new_version = JSON.parse(
        readFileSync(`${testdir}/new_version`),
      ).panelapp_host;

      expect(new_version).not.toBe(old_version);
      expect(new_version).toBe("unknown");
    });

    test("panelapp_host is shortened to hostname", () => {
      aws_sync({ PANELAPP_HOSTNAME: "panelapp-dev.genomicsengland.co.uk" });
      aws(`s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/version"`);

      const version = JSON.parse(
        readFileSync(`${testdir}/version`),
      ).panelapp_host;

      expect(version).toBe("panelapp-dev");
    });
  });

  describe("last data sync timestamp", () => {
    test("last_data_sync is updated", () => {
      const now = new Date().toISOString();

      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      const old_last_sync = JSON.parse(
        readFileSync(`${testdir}/old_version`),
      ).last_data_sync;
      const new_last_sync = JSON.parse(
        readFileSync(`${testdir}/new_version`),
      ).last_data_sync;

      expectTimestamp(old_last_sync).toBeBefore(now);
      expectTimestamp(now).toBeBefore(new_last_sync);
    });
  });

  describe("data changed timestamp", () => {
    test("data_changed is not updated", () => {
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      const old_data = JSON.parse(readFileSync(`${testdir}/old_version`));
      const new_data = JSON.parse(readFileSync(`${testdir}/new_version`));

      expect(new_data.data_changed).toBe(old_data.data_changed);
      expect(new_data.last_data_sync).not.toBe(old_data.last_data_sync);
    });

    test("data_changed is updated when file is modified", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      writeFileSync(`${upload_dir}/index.html`, randomUUID());
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      expectTimestamp(
        JSON.parse(readFileSync(`${testdir}/old_version`)).data_changed,
      ).toBeBefore(
        JSON.parse(readFileSync(`${testdir}/new_version`)).data_changed,
      );
    });

    test("data_changes is updated when file is added", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      writeFileSync(`${upload_dir}/new_file.txt`, randomUUID());
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      expectTimestamp(
        JSON.parse(readFileSync(`${testdir}/old_version`)).data_changed,
      ).toBeBefore(
        JSON.parse(readFileSync(`${testdir}/new_version`)).data_changed,
      );
    });

    test("data_changes is updated when file is removed", () => {
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/old_version"`,
      );
      rmSync(`${upload_dir}/index.html`);
      aws_sync();
      aws(
        `s3 cp "s3://${bucket}/${VERSION_FILENAME}" "${testdir}/new_version"`,
      );

      expectTimestamp(
        JSON.parse(readFileSync(`${testdir}/old_version`)).data_changed,
      ).toBeBefore(
        JSON.parse(readFileSync(`${testdir}/new_version`)).data_changed,
      );
    });
  });

  describe("content is uploaded", () => {
    test("changed content is uploaded", () => {
      writeFileSync(`${upload_dir}/index.html`, randomUUID());
      aws_sync();
      aws(`s3 cp "s3://${bucket}/index.html" "${testdir}/new_index"`);

      expect(readFileSync(`${testdir}/new_index`).toString()).toBe(
        readFileSync(`${upload_dir}/index.html`).toString(),
      );
    });

    test("added file is uploaded", () => {
      writeFileSync(`${upload_dir}/new_file.txt`, randomUUID());
      aws_sync();
      aws(`s3 cp "s3://${bucket}/new_file.txt" "${testdir}/new_file.txt"`);

      expect(readFileSync(`${testdir}/new_file.txt`).toString()).toBe(
        readFileSync(`${upload_dir}/new_file.txt`).toString(),
      );
    });

    test("deleted file is removed", () => {
      rmSync(`${upload_dir}/index.html`);
      aws_sync();
      const listing = aws(`s3 ls "s3://${bucket}/"`).toString();

      expect(listing).toMatch(/pretty\.css/);
      expect(listing).not.toMatch(/index\.html/);
    });
  });

  describe("error handling", () => {
    test("return code is zero for success", () => {
      const child = spawnSync("./aws_sync.js", [upload_dir, bucket], { env });
      expect(child.status).toBe(0);
    });

    test("return code is non-zero for errors", () => {
      const child = spawnSync("./aws_sync.js", [upload_dir, "unknown-bucket"], {
        env,
      });
      expect(child.status).not.toBe(0);
    });
  });
});
