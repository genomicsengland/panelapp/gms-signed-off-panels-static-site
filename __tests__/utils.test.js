import apiPanelData from "../src/fixtures/api/panels/25/v2.4.json";
import results1 from "../src/fixtures/api/panels/results-1.json";
import results2 from "../src/fixtures/api/panels/results-2.json";
import panel145 from "../src/fixtures/data/panels/145/v2.2.json";
import panel25 from "../src/fixtures/data/panels/25/v2.4.json";
import panel285 from "../src/fixtures/data/panels/285/v3.3.json";
import panel484 from "../src/fixtures/data/panels/484/v2.3.json";
import panel486 from "../src/fixtures/data/panels/486/v14.43.json";
import panel847 from "../src/fixtures/data/panels/847/v1.58.json";
import entities from "../src/fixtures/gatsby-node-entities.json";
import {
  getEntities,
  getEntitiesData,
  getEntityPanel,
  getPanelData,
  getPanelSubpanels,
  getPanelsData,
  getSubpanels,
} from "../utils";

import axios from "axios";
import fs from "fs";
import promises from "fs/promises";
jest.mock("fs");
jest.mock("fs/promises");
jest.mock("axios");

beforeEach(() => {
  axios.get.mockReset();
  fs.existsSync.mockReset();
  promises.readFile.mockReset();
  fs.mkdirSync.mockReset();
  fs.writeFileSync.mockReset();
});

const reporter = { info: jest.fn() };

describe("getPanelsData()", () => {
  test("Unsuccessful request", async () => {
    axios.get.mockImplementation(async () => {
      throw new Error("Request failed with status code 500");
    });

    await expect(getPanelsData(reporter)).rejects.toThrow(
      "Request failed with status code 500",
    );
  });

  test("Empty results", async () => {
    axios.get.mockImplementation(async () => ({
      data: {
        count: 0,
        next: null,
        previous: null,
        results: [],
      },
    }));

    const panelsData = await getPanelsData(reporter);
    expect(panelsData).toEqual([]);
  });

  test("Results without next page", async () => {
    axios.get.mockImplementation(async () => ({
      data: {
        count: 3,
        next: null,
        previous: null,
        results: results1,
      },
    }));

    const panelsData = await getPanelsData(reporter);
    expect(panelsData).toEqual(
      [...results1]
        .map(panelSummary => ({
          id: panelSummary.id,
          name: panelSummary.name,
          version: panelSummary.version,
          signed_off: panelSummary.signed_off,
          relevant_disorders: panelSummary.relevant_disorders,
          types: panelSummary.types.map(type => ({ name: type.name })),
          stats: {
            number_of_genes: panelSummary.stats.number_of_genes,
          },
        }))
        .sort((p1, p2) =>
          p1.name.toLowerCase().localeCompare(p2.name.toLowerCase()),
        ),
    );
  });

  test("Results with next page", async () => {
    axios.get.mockImplementation(async url =>
      url === "/next-page"
        ? {
            data: {
              count: 3,
              next: null,
              previous: "/first-page",
              results: results2,
            },
          }
        : {
            data: {
              count: 3,
              next: "/next-page",
              previous: null,
              results: results1,
            },
          },
    );

    const panelsData = await getPanelsData(reporter);
    expect(panelsData).toEqual(
      [...results1, ...results2]
        .map(panelSummary => ({
          id: panelSummary.id,
          name: panelSummary.name,
          version: panelSummary.version,
          signed_off: panelSummary.signed_off,
          relevant_disorders: panelSummary.relevant_disorders,
          types: panelSummary.types.map(type => ({ name: type.name })),
          stats: {
            number_of_genes: panelSummary.stats.number_of_genes,
          },
        }))
        .sort((p1, p2) =>
          p1.name.toLowerCase().localeCompare(p2.name.toLowerCase()),
        ),
    );
  });
});

describe("getPanelData()", () => {
  test("Unsuccessful request", async () => {
    fs.existsSync.mockImplementation(() => false);
    axios.get.mockImplementation(async () => {
      throw new Error("Request failed with status code 500");
    });

    await expect(getPanelsData(reporter)).rejects.toThrow(
      "Request failed with status code 500",
    );
  });

  test("Request with cache", async () => {
    promises.readFile.mockImplementation(
      () => new Promise(resolve => resolve(JSON.stringify(panel25))),
    );

    const id = 25;
    const version = "2.4";

    fs.existsSync.mockImplementation(() => true);

    const panelData = await getPanelData(id, version);
    expect(panelData).toEqual(panel25);

    expect(fs.existsSync).toHaveBeenCalledTimes(1);
    expect(fs.existsSync).toHaveBeenCalledWith(
      `./data/panels/${id}/v${version}.json`,
    );
  });

  test("Request without cache", async () => {
    const id = 399;
    const version = "2.4";

    fs.existsSync.mockImplementation(() => false);
    axios.get.mockImplementation(async () => ({ data: apiPanelData }));

    const panelData = await getPanelData(id, version);
    expect(panelData).toEqual(panel25);

    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(
      `https://panelapp.genomicsengland.co.uk/api/v1/panels/${id}/?version=${version}`,
    );

    expect(fs.mkdirSync).toHaveBeenCalledTimes(1);
    expect(fs.mkdirSync).toHaveBeenCalledWith(`./data/panels/${id}`, {
      recursive: true,
    });
    expect(fs.writeFileSync).toHaveBeenCalledTimes(1);
    expect(fs.writeFileSync).toHaveBeenCalledWith(
      `./data/panels/${id}/v${version}.json`,
      JSON.stringify(panelData),
    );
  });
});

test("getEntityPanel()", () => {
  const panelData = panel25;
  const panelEntity = panel25.entities[0];

  expect(getEntityPanel(panelData, panelEntity)).toEqual({
    id: panelData.panel.id,
    name: panelData.panel.name,
    version: panelData.panel.version,
    relevant_disorders: panelData.panel.relevant_disorders,
    entity: {
      confidence_level: panelEntity.confidence_level,
      mode_of_inheritance: panelEntity.mode_of_inheritance,
      phenotypes: panelEntity.phenotypes,
    },
  });
});

test("getEntitiesData()", () => {
  const entitiesData = getEntitiesData(entities);
  expect(entitiesData).toEqual({
    entities: [
      {
        entity_name: "AAAS",
        entity_type: "gene",
      },
      {
        entity_name: "AARS",
        entity_type: "gene",
      },
    ],
  });
});

describe("getSubpanels()", () => {
  test("with a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    const panelsData = [
      {
        id: 486,
        name: "Paediatric disorders",
        version: "14.43",
      },
    ];
    promises.readFile.mockImplementation(
      path =>
        new Promise(resolve =>
          resolve(
            {
              "./data/panels/486/v14.43.json": JSON.stringify(panel486),
              "./data/panels/484/v2.3.json": JSON.stringify(panel484),
              "./data/panels/285/v3.3.json": JSON.stringify(panel285),
            }[path],
          ),
        ),
    );
    const subpanels = await getSubpanels(panelsData);
    const subpanelNames = Object.keys(subpanels);

    expect(subpanelNames.length).toBe(2);
    expect(subpanels["DDG2P"].superpanels).toEqual(["Paediatric disorders"]);
  });

  test("without a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    const panelsData = [
      {
        id: 285,
        name: "Intellectual disability",
        version: "3.3",
      },
    ];
    promises.readFile.mockImplementation(
      path => new Promise(resolve => resolve(JSON.stringify(panel285))),
    );

    const subpanels = await getSubpanels(panelsData);
    expect(subpanels).toEqual({});
  });
});

describe("getEntities()", () => {
  test("with a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    promises.readFile.mockImplementation(
      path => new Promise(resolve => resolve(JSON.stringify(panel486))),
    );

    const panelsData = [
      {
        id: 486,
        name: "Paediatric disorders",
        version: "14.43",
      },
    ];
    const entities = await getEntities(panelsData);
    expect(entities).toEqual({});
  });

  test("without a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    promises.readFile.mockImplementation(
      path =>
        new Promise(resolve =>
          resolve(
            {
              "./data/panels/847/v1.58.json": JSON.stringify(panel847),
              "./data/panels/145/v2.2.json": JSON.stringify(panel145),
              "./data/panels/285/v3.3.json": JSON.stringify(panel285),
            }[path],
          ),
        ),
    );
    const panelsData = [
      {
        // Amber panel for AAAS
        id: 847,
        name: "Childhood onset dystonia or chorea or related movement disorder",
        version: "1.58",
      },
      {
        // Green panel for AAAS
        id: 285,
        name: "Intellectual disability",
        version: "3.3",
      },
      {
        // Green panel for AAAS
        id: 145,
        name: "Congenital adrenal hypoplasia",
        version: "2.2",
      },
    ];
    const entities = await getEntities(panelsData);
    const entityNames = Object.keys(entities);
    const aaas = entities["AAAS"];

    expect(entityNames.length).toBe(1);
    expect(aaas.panels.map(x => x.name)).toStrictEqual([
      "Congenital adrenal hypoplasia",
      "Intellectual disability",
    ]);
  });
});

describe("getPanelSubpanels()", () => {
  test("with superpanel", () => {
    const panelData = {
      entities: [
        {
          entity_name: "AAA1",
          panel: {
            id: 100,
            name: "Inherited disorder",
            version: "3.4",
          },
        },
      ],
    };

    const panelSubpanels = getPanelSubpanels(panelData);
    expect(panelSubpanels).toEqual({
      "Inherited disorder": {
        id: 100,
        version: "3.4",
      },
    });
  });

  test("without superpanel", () => {
    const panelData = {
      entities: [
        {
          entity_name: "AAA1",
        },
      ],
    };

    const panelSubpanels = getPanelSubpanels(panelData);
    expect(panelSubpanels).toEqual({});
  });
});
