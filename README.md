# GMS Signed Off Panels Static Site

Static site archive for GMS Signed Off panels.

## Overview

The app is built with GatsbyJS + TypeScript. It uses JestJS for testing and TailwindCSS for styling.

The available pages are:

- `/`

(defined in `src/pages`)

and:

- `/panels`
- `/panels/<panel_id>/v<panel_version>`

(defined in `gatsby-node.js`)

## Instructions

To install the dependencies:

```shell
npm install
```

To run the tests:

```shell
npm test
```

To run the development server:

```shell
npm run develop
```

To build and serve the production website:

```shell
npm run build
npm run serve
```

## Syncing

GatsbyJS builds the app locally. It is then uploaded to AWS S3 bucket with
`aws_sync.js`, which is a standalone Node.js script.

For testing, start a localstack instance:

```shell
npm run localstack
```

Then run the tests:

```shell
npm run test:aws_sync
```
