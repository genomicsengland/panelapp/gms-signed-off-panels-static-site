//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React, { useState } from "react";

import Layout from "../layouts/Layout";
import EntitiesList from "../components/EntitiesList";
import EntitiesSearchBox from "../components/EntitiesSearchBox";

interface EntitiesTemplateProps {
  pageContext: {
    entities: {
      entity_name: string;
      entity_type: string;
    }[];
  };
}

export default function EntitiesTemplate({
  pageContext: { entities },
}: EntitiesTemplateProps) {
  const initialEntities = entities;
  const [currentEntities, setEntities] = useState(initialEntities);

  function onChange() {
    const searchBar = document.querySelector("#search-bar");
    const genesCheckbox = document.querySelector("#genes");
    const strCheckbox = document.querySelector("#strs");
    const regionsCheckbox = document.querySelector("#regions");
    const query = searchBar.value.toLowerCase();

    const filteredEntities = initialEntities
      .filter(entity => {
        return (
          (entity.entity_type === "gene" && genesCheckbox.checked) ||
          (entity.entity_type === "str" && strCheckbox.checked) ||
          (entity.entity_type === "region" && regionsCheckbox.checked)
        );
      })
      .filter(entity => {
        const entityName = entity.entity_name.toLowerCase();
        return entityName.startsWith(query);
      });

    setEntities(filteredEntities);
  }

  return (
    <Layout>
      <div>
        <h3 className="my-4 text-3xl font-bold">
          {entities.length} genes and genomic entities
        </h3>
        <div className="border rounded-l m-2">
          <div className="bg-nhs-pale-grey px-4 py-3">
            Find a gene or genomic entity
          </div>
          <div className="p-4">
            <EntitiesSearchBox entities={currentEntities} onChange={onChange} />
            <hr className="my-5" />
            <EntitiesList entities={currentEntities} />
          </div>
        </div>
      </div>
    </Layout>
  );
}
