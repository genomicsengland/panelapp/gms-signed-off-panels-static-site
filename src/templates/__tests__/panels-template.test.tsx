/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";

import panels from "../../fixtures/panels.json";
import subpanels from "../../fixtures/subpanels.json";
import PanelsTemplate from "../panels-template";

describe("<PanelsTemplate />", () => {
  test("Search for existing name", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />,
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "intellectual");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing name", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />,
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "intellectualxxx");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for existing R-number", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />,
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "r10");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing R-number", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />,
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "r999");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for existing type", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />,
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "100k");

    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing type", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />,
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "999k");
    expect(panelsTemplate.container).toMatchSnapshot();
  });
});
