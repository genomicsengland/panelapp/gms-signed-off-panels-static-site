/**
 * @jest-environment jsdom
 */

import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";

import entities from "../../fixtures/all-entities.json";
import EntitiesTemplate from "../entities-template";

function setup() {
  const entitiesTemplate = render(
    <EntitiesTemplate pageContext={{ entities }} />,
  );

  return { entitiesTemplate };
}

test("Search for non-existing entities", async () => {
  const { entitiesTemplate } = setup();
  expect(entitiesTemplate.container).toMatchSnapshot();

  await userEvent.type(screen.getByRole("textbox"), "atxnxxx");
  expect(entitiesTemplate.container).toMatchSnapshot();
});

describe("<EntitiesTemplate />", () => {
  test("Search for existing entities", async () => {
    const { entitiesTemplate } = setup();
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "atxn");
    expect(entitiesTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing entities", async () => {
    const { entitiesTemplate } = setup();
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.type(screen.getByRole("textbox"), "atxnxxx");
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
  test("Test Genes checkbox", async () => {
    const { entitiesTemplate } = setup();
    const genesCheckbox = screen.getByRole("checkbox", { name: "Genes" });
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.click(genesCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.click(genesCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
  test("Test STRs checkbox", async () => {
    const { entitiesTemplate } = setup();
    const strsCheckbox = screen.getByRole("checkbox", { name: "STRs" });
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.click(strsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.click(strsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
  test("Test Regions checkbox", async () => {
    const { entitiesTemplate } = setup();
    const regionsCheckbox = screen.getByRole("checkbox", { name: "Regions" });
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.click(regionsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();

    await userEvent.click(regionsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
});
