/**
 * @jest-environment jsdom
 */

import {
  getAllByRole,
  queryByRole,
  render,
  screen,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";

import entity from "../../fixtures/entity-gene.json";
import EntityTemplate from "../entity-template";

test("<EntityTemplate />", async () => {
  render(<EntityTemplate pageContext={{ entity, subpanels: {} }} />);
  const panelFilterInput = screen.getByRole("textbox");

  const panelTable = screen.getByRole("table");

  // Search for existing panels
  await userEvent.type(panelFilterInput, "disorder");

  expect(
    getAllByRole<HTMLAnchorElement>(panelTable, "link").map(e => e.href),
  ).toEqual([
    "http://localhost/panels/847/v1.58",
    "http://localhost/panels/474/v2.31",
  ]);

  // Search for non-existing panels
  await userEvent.type(panelFilterInput, "disorderxxx");

  expect(queryByRole(panelTable, "link")).not.toBeInTheDocument();
});
