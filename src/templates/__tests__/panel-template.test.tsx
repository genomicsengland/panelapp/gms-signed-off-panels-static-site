import React from "react";
import { create } from "react-test-renderer";

import entities from "../../fixtures/entities.json";
import panel from "../../fixtures/panel-1.json";
import PanelTemplate from "../panel-template";

test("<PanelTemplate />", () => {
  const panelTemplate = create(
    <PanelTemplate pageContext={{ panel, entities, versions: [] }} />,
  );
  expect(panelTemplate.toJSON()).toMatchSnapshot();
});
