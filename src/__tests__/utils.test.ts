import { isRNumber } from "../utils";

test("isRNumber()", () => {
  expect(isRNumber("abc")).toBe(false);
  expect(isRNumber("123")).toBe(false);
  expect(isRNumber("R")).toBe(false);
  expect(isRNumber("Rabc")).toBe(false);
  expect(isRNumber("Rabc.")).toBe(false);
  expect(isRNumber("R123.")).toBe(false);
  expect(isRNumber("R123.abc")).toBe(false);

  expect(isRNumber("R123")).toBe(true);
  expect(isRNumber("R123.123")).toBe(true);
});
