/**
 * @jest-environment jsdom
 */

import { fireEvent, render } from "@testing-library/react";
import { navigate } from "gatsby";
import React from "react";
import { create } from "react-test-renderer";

import entities from "../../fixtures/entities.json";
import panel1 from "../../fixtures/panel-1.json";
import panel2 from "../../fixtures/panel-2.json";
import panel3 from "../../fixtures/panel-3.json";
import PanelHeader from "../PanelHeader";

jest.mock("gatsby", () => {
  const gatsby = jest.requireActual("gatsby");

  return {
    ...gatsby,
    navigate: jest.fn(),
  };
});

describe("<PanelHeader />", () => {
  test("with relevant disorders and types", () => {
    const panelHeader = create(
      <PanelHeader
        panel={panel1}
        entities={entities}
        versions={[panel1.version]}
      />,
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("without relevant disorders and types", () => {
    const panelHeader = create(
      <PanelHeader
        panel={panel2}
        entities={entities}
        versions={[panel2.version]}
      />,
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("with superpanel", () => {
    const panelHeader = create(
      <PanelHeader
        panel={panel3}
        entities={entities}
        versions={[panel3.version]}
      />,
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("without entities", () => {
    const panelHeader = create(
      <PanelHeader panel={panel1} entities={[]} versions={[panel1.version]} />,
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("navigates to another version", () => {
    navigate.mockReset();
    const { getByTestId } = render(
      <PanelHeader
        panel={panel1}
        entities={[]}
        versions={["3.2", "3.1", "3.0"]}
      />,
    );
    fireEvent.change(getByTestId("selectVersion"), {
      target: { value: "3.0" },
    });
    expect(navigate.mock.calls).toMatchInlineSnapshot(`
      [
        [
          "/panels/123/v3.0",
        ],
      ]
    `);
  });

  test("old version includes warning banner", () => {
    const { getByText } = render(
      <PanelHeader
        panel={panel1}
        entities={[]}
        versions={["3.5", "3.2", "3.1", "3.0"]}
      />,
    );
    expect(
      getByText("There is a more recent signed-off version of this panel"),
    ).toBeInTheDocument();
  });

  test("natural versions order", () => {
    const { container } = render(
      <PanelHeader
        panel={panel1}
        entities={[]}
        versions={["3.5", "3.2", "3.1", "3.11", "3.0"]}
      />,
    );
    const allOptions = container.querySelectorAll("select option");
    const versions = Array.from(allOptions).map(o => o.getAttribute("value"));
    expect(versions).toMatchInlineSnapshot(`
      [
        "3.11",
        "3.5",
        "3.2",
        "3.1",
        "3.0",
      ]
    `);
  });
});
