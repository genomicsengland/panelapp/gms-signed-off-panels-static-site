import React from "react";
import { create } from "react-test-renderer";

import SuperPanelInfo from "../SuperPanelInfo";

test("<SuperPanelInfo />", () => {
  const subpanels = {
    "Intellectual disability": {
      id: 285,
      version: "3.3",
    },
    "Inborn errors of metabolism": {
      id: 467,
      version: "2.5",
    },
  };
  const superPanelInfo = create(<SuperPanelInfo subpanels={subpanels} />);
  expect(superPanelInfo.toJSON()).toMatchSnapshot();
});
