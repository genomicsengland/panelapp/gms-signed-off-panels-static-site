import React from "react";
import { create } from "react-test-renderer";

import PanelTable from "../PanelTable";
import entities from "../../fixtures/entities.json";

test("<PanelTable />", () => {
  const panelTable = create(<PanelTable entities={entities} />);
  expect(panelTable.toJSON()).toMatchSnapshot();
});
