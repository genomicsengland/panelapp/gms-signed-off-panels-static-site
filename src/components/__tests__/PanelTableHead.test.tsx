import React from "react";
import { create } from "react-test-renderer";

import PanelTableHead from "../PanelTableHead";

test("<PanelTableHead />", () => {
  const panelTableHead = create(<PanelTableHead />);
  expect(panelTableHead.toJSON()).toMatchSnapshot();
});
