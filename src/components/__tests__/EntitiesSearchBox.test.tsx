import React from "react";
import { create } from "react-test-renderer";

import entities from "../../fixtures/all-entities.json";
import EntitiesSearchBox from "../EntitiesSearchBox";

test("<EntitiesSearchBox />", () => {
  const entitiesSearchBox = create(
    <EntitiesSearchBox entities={entities} onSearchBarChange={() => {}} />,
  );
  expect(entitiesSearchBox.toJSON()).toMatchSnapshot();
});
