import React from "react";
import { create } from "react-test-renderer";

import panels from "../../fixtures/panels.json";
import subpanels from "../../fixtures/subpanels.json";
import PanelsTableBody from "../PanelsTableBody";

test("<PanelsTableBody />", () => {
  const panelsTableBody = create(
    <PanelsTableBody panels={panels} subpanels={subpanels} />,
  );
  expect(panelsTableBody.toJSON()).toMatchSnapshot();
});
