import React from "react";
import { create } from "react-test-renderer";

import panels from "../../fixtures/panels.json";
import subpanels from "../../fixtures/subpanels.json";
import PanelsTable from "../PanelsTable";

test("<PanelsTable />", () => {
  const panelsTable = create(
    <PanelsTable
      panels={panels}
      subpanels={subpanels}
      onSearchBarChange={() => {}}
    />,
  );
  expect(panelsTable.toJSON()).toMatchSnapshot();
});
