import React from "react";
import { create } from "react-test-renderer";

import EntityHeader from "../EntityHeader";
import entity1 from "../../fixtures/entity-gene.json";
import entity2 from "../../fixtures/entity-region.json";

describe("<EntityHeader />", () => {
  test("with gene entity with gene data", () => {
    const entityHeader = create(<EntityHeader entity={entity1} />);
    expect(entityHeader.toJSON()).toMatchSnapshot();
  });

  test("with region entity without gene data", () => {
    const entityHeader = create(<EntityHeader entity={entity2} />);
    expect(entityHeader.toJSON()).toMatchSnapshot();
  });
});
