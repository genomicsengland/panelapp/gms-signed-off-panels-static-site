import React from "react";
import { create } from "react-test-renderer";

import entity from "../../fixtures/entity-gene.json";
import EntityTableHead from "../EntityTableHead";

describe("<EntityTableHead />", () => {
  test("with 1 panel", () => {
    const panels = [entity.panels[0]];
    const entityTableHead = create(
      <EntityTableHead panels={panels} onSearchBarChange={() => {}} />,
    );
    expect(entityTableHead.toJSON()).toMatchSnapshot();
  });

  test("with more than 1 panel", () => {
    const panels = entity.panels;
    const entityTableHead = create(
      <EntityTableHead panels={panels} onSearchBarChange={() => {}} />,
    );
    expect(entityTableHead.toJSON()).toMatchSnapshot();
  });
});
