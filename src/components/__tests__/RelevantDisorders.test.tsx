import React from "react";
import { create } from "react-test-renderer";

import RelevantDisorders from "../RelevantDisorders";

test("<RelevantDisorders />", () => {
  const rds = ["R100", "R101"];
  const relevantDisorders = create(<RelevantDisorders rds={rds} />);
  expect(relevantDisorders.toJSON()).toMatchSnapshot();
});
