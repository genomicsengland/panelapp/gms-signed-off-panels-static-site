//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import { Link } from "gatsby";
import React from "react";

import EntityPanel from "../interfaces/EntityPanel";
import Subpanels from "../interfaces/Subpanels";
import { getConfidenceMetadata, isRNumber } from "../utils";

interface EntityTableBodyProps {
  panels: EntityPanel[];
  subpanels: Subpanels;
}

export default function EntityTableBody({
  panels,
  subpanels,
}: EntityTableBodyProps) {
  return (
    <tbody className="align-top">
      {panels.map(panel => {
        const [name, styles] = getConfidenceMetadata(
          panel.entity.confidence_level,
        );
        const { id, version, name: panelName } = panel;
        const rNumbers = panel.relevant_disorders.filter(isRNumber);

        return (
          <tr key={id}>
            <td className="border px-4 py-2">
              <div className="text-lg font-semibold mb-2">
                <div
                  className={`inline-block text-sm border rounded px-6 ${styles}`}
                >
                  {name}
                </div>{" "}
                in{" "}
                {
                  <Link
                    to={`/panels/${id}/v${version}`}
                    className="text-nhs-blue hover:underline"
                  >
                    {panelName}
                  </Link>
                }
              </div>
              {subpanels[panelName] && (
                <>
                  <div className="mb-1">
                    Component of the following Super Panels:
                  </div>
                  <ul className="mb-2">
                    {subpanels[panelName].superpanels.map(superpanelName => (
                      <li key={superpanelName}>- {superpanelName}</li>
                    ))}
                  </ul>
                </>
              )}
              {!!rNumbers.length && (
                <div className="mb-1">
                  R-numbers:{" "}
                  {rNumbers.map((rNumber, i) => (
                    <span key={rNumber}>
                      <span className="font-bold">{rNumber}</span>
                      {i < rNumbers.length - 1 && ", "}
                    </span>
                  ))}
                </div>
              )}
              <div>Signed-off version {version}</div>
            </td>
            <td className="border px-4 py-2">
              {panel.entity.mode_of_inheritance}
            </td>
            <td className="border px-4 py-2">
              <div className="font-bold">Phenotypes</div>
              <div>
                {panel.entity.phenotypes
                  ? panel.entity.phenotypes.join(", ")
                  : "N/A"}
              </div>
            </td>
          </tr>
        );
      })}
    </tbody>
  );
}
