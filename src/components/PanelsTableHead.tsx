//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React, { ChangeEvent } from "react";

import PanelSummary from "../interfaces/PanelSummary";

interface OnSearchBarChange {
  (event: ChangeEvent<HTMLInputElement>): void;
}

interface PanelsTableHeadProps {
  panels: PanelSummary[];
  searchField: React.MutableRefObject<null>;
  onSearchBarChange: OnSearchBarChange;
}

export default function PanelsTableHead({
  panels,
  searchField,
  onSearchBarChange,
}: PanelsTableHeadProps) {
  return (
    <thead>
      <tr className="bg-nhs-pale-grey">
        <th className="border px-4 py-2 text-left">Panel</th>
      </tr>
      <tr>
        <td className="border">
          <div className="border rounded-l m-2 h-10 bg-nhs-pale-grey">
            <input
              type="text"
              id="panel-filter"
              ref={searchField}
              placeholder="Filter panels (by name, type or R-number)"
              autoComplete="off"
              onChange={onSearchBarChange}
              className="border w-5/6 h-full py-2 px-3"
            />
            <span className="ml-2">
              {panels.length} {panels.length === 1 ? "panel" : "panels"}
            </span>
          </div>
        </td>
      </tr>
    </thead>
  );
}
