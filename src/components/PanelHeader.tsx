//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import { navigate } from "gatsby";
import orderBy from "lodash/orderBy";
import { DateTime } from "luxon";
import React from "react";

import Entity from "../interfaces/Entity";
import Panel from "../interfaces/Panel";
import PanelNotice from "./PanelNotice";
import RelevantDisorders from "./RelevantDisorders";
import SuperPanelInfo from "./SuperPanelInfo";

interface PanelHeaderProps {
  panel: Panel;
  entities: Entity[];
  versions: string[];
}

function orderVersions(allVersions: string[]) {
  return orderBy(
    allVersions.map(v =>
      v
        .replace("v", "")
        .split(".")
        .map(p => Number.parseInt(p, 10)),
    ),
    [0, 1],
    ["desc", "desc"],
  ).map(v => v.join("."));
}

export default function PanelHeader({
  panel,
  entities,
  versions,
}: PanelHeaderProps) {
  const isSuperPanel = !!Object.keys(panel.subpanels).length;

  const orderedVersions = orderVersions(versions);
  const [latestVersion] = orderedVersions;

  const selectVersion = React.useCallback(
    (ev: React.ChangeEvent<HTMLSelectElement>) => {
      ev.preventDefault();
      const version = ev.target.value;

      navigate(`/panels/${panel.id}/v${version}`);
    },
    [panel],
  );

  return (
    <div className="mt-6 w-3/4">
      <h3 className="mb-4 text-3xl font-bold">
        {panel.name} (Version:{" "}
        <select
          onChange={selectVersion}
          data-testid="selectVersion"
          defaultValue={panel.version}
        >
          {orderedVersions.map(version => (
            <option value={version} key={version}>
              {version}
            </option>
          ))}
        </select>
        )
      </h3>
      <PanelNotice panelId={panel.id} />
      {panel.version === latestVersion || (
        <div className="my-2 bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative">
          There is a more recent signed-off version of this panel
        </div>
      )}
      <div className="text-xl font-light">
        {!!panel.relevant_disorders.length && (
          <div className="mb-2">
            <span className="font-semibold">Relevant disorders: </span>
            <RelevantDisorders rds={panel.relevant_disorders} />
          </div>
        )}
        {isSuperPanel && <SuperPanelInfo subpanels={panel.subpanels} />}
        <div className="mb-2">
          <span className="text-xl font-semibold">Signed off date: </span>
          {DateTime.fromISO(panel.signed_off, {
            locale: "en-GB",
          }).toLocaleString(DateTime.DATE_MED)}
        </div>
        {!!panel.types.length && (
          <div className="mb-2">
            <span className="text-xl font-semibold">Panel types: </span>
            {panel.types.map(type => type.name).join(", ")}
          </div>
        )}
        <div className="mb-3">
          <a
            href={`https://panelapp.genomicsengland.co.uk/panels/${panel.id}/`}
            target="_blank"
            rel="noreferrer"
            className="text-nhs-blue hover:underline"
          >
            See this panel in PanelApp
          </a>
        </div>
        <div className="mb-5">
          <div className="text-2xl font-semibold mb-1">
            {/* Show the total number of unique entities */}
            {
              [...new Set(entities.map(entity => entity.entity_name))].length
            }{" "}
            {isSuperPanel && "unique"} green entities
          </div>
        </div>
      </div>
    </div>
  );
}
