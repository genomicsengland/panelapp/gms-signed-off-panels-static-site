//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";

import EntitySummary from "../interfaces/EntitySummary";
import EntityPanel from "../interfaces/EntityPanel";

interface EntityHeaderProps {
  entity: {
    data: EntitySummary;
    panels: EntityPanel[];
  };
}

export default function EntityHeader({ entity }: EntityHeaderProps) {
  return (
    <>
      <h3 className="mb-2 text-3xl font-bold">{entity.data.entity_name}</h3>
      <div className="text-xl mb-4">
        {entity.data.entity_type === "region" && entity.data.verbose_name}
        {entity.data.gene_data && (
          <div className="mb-1">{entity.data.gene_data.gene_name}</div>
        )}
        {entity.data.gene_data && entity.data.gene_data.omim_gene && (
          <div className="text-nhs-blue mb-2">
            OMIM:{" "}
            {entity.data.gene_data.omim_gene.map((omim, i) => (
              <span key={i}>
                <a
                  href={`https://omim.org/entry/${omim}`}
                  target="_blank"
                  rel="noreferrer"
                  className="hover:underline"
                >
                  {omim}
                </a>
                {i !== entity.data.gene_data.omim_gene.length - 1 ? ", " : null}
              </span>
            ))}
          </div>
        )}
        <div>
          <a
            href={`https://panelapp.genomicsengland.co.uk/panels/entities/${entity.data.entity_name}`}
            target="_blank"
            rel="noreferrer"
            className="font-light text-nhs-blue hover:underline"
          >
            See this entity in PanelApp
          </a>
        </div>
      </div>
    </>
  );
}
