import React from "react";

function CancerPanelNotice() {
  return (
    <div className="my-2 bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative">
      This cancer panel will be applied as appropriate in the GMS cancer
      pipeline and is not requestable separately.
    </div>
  );
}

export default CancerPanelNotice;
