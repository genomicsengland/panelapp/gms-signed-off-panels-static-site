import React from "react";

function RareDiseasePanelNotice() {
  return (
    <div className="my-2 bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative">
      This panel is a component of a whole genome sequencing super panel and is
      only requestable via the superpanel.
    </div>
  );
}

export default RareDiseasePanelNotice;
