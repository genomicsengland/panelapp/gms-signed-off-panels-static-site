import React, { useContext } from "react";
import CancerPanelNotice from "./CancerPanelNotice";
import PanelMetadataContext from "./Context";
import RareDiseasePanelNotice from "./RareDiseasePanelNotice";

interface PanelNoticeWrapperProps {
  panelId: number;
}

interface PanelNoticeProps {
  panelId: number;
  gmsPanelNoticeRd?: number[];
  gmsPanelNoticeCancer?: number[];
}

function PanelNotice({
  panelId,
  gmsPanelNoticeRd,
  gmsPanelNoticeCancer,
}: PanelNoticeProps) {
  if (gmsPanelNoticeRd?.includes(panelId)) {
    return <RareDiseasePanelNotice />;
  }

  if (gmsPanelNoticeCancer?.includes(panelId)) {
    return <CancerPanelNotice />;
  }

  return null;
}

function PanelNoticeWrapper(props: PanelNoticeWrapperProps) {
  const { gmsPanelNoticeRd, gmsPanelNoticeCancer } =
    useContext(PanelMetadataContext);
  return (
    <PanelNotice
      panelId={props.panelId}
      gmsPanelNoticeRd={gmsPanelNoticeRd}
      gmsPanelNoticeCancer={gmsPanelNoticeCancer}
    />
  );
}

export default PanelNoticeWrapper;
