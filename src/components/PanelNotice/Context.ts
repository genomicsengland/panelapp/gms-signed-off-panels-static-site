import { createContext } from "react";

export type PanelMetadataContextProps = {
  gmsPanelNoticeRd?: number[];
  gmsPanelNoticeCancer?: number[];
};

export const PanelMetadataContext = createContext<PanelMetadataContextProps>({
  gmsPanelNoticeRd: [],
  gmsPanelNoticeCancer: [],
});
PanelMetadataContext.displayName = "PanelMetadataContext";

export default PanelMetadataContext;
