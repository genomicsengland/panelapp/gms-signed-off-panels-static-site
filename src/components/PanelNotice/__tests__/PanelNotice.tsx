import React from "react";
import renderer from "react-test-renderer";
import PanelMetadataContext from "../Context";
import PanelNotice from "../PanelNotice";

it("Renders notice for RD panels", () => {
  const tree = renderer
    .create(
      <PanelMetadataContext.Provider
        value={{
          gmsPanelNoticeRd: [1],
          gmsPanelNoticeCancer: [2],
        }}
      >
        <PanelNotice panelId={1} />
      </PanelMetadataContext.Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it("Renders notice for cancer panels", () => {
  const tree = renderer
    .create(
      <PanelMetadataContext.Provider
        value={{
          gmsPanelNoticeRd: [1],
          gmsPanelNoticeCancer: [2],
        }}
      >
        <PanelNotice panelId={2} />
      </PanelMetadataContext.Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it("Doesn't render notice for any other panel", () => {
  const tree = renderer
    .create(
      <PanelMetadataContext.Provider
        value={{
          gmsPanelNoticeRd: [1],
          gmsPanelNoticeCancer: [2],
        }}
      >
        <PanelNotice panelId={3} />
      </PanelMetadataContext.Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
