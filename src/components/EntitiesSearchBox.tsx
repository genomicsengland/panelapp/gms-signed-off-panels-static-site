//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";

interface EntitiesSearchBoxProps {
  entities: {
    entity_name: string;
    entity_type: string;
  }[];
  onChange: () => void;
}

export default function EntitiesSearchBox({
  entities,
  onChange,
}: EntitiesSearchBoxProps) {
  return (
    <>
      <div className="font-bold">Gene or Genomic Entity Name</div>
      <div className="text-nhs-dark-grey mb-2">
        <label htmlFor="search-bar">
          Enter a gene symbol, STR name, Region name, or the beginning of one,
          eg "CD" or "CD19"
        </label>
      </div>
      <div className="flex h-10 bg-nhs-pale-grey">
        <input
          type="text"
          name="search-bar"
          id="search-bar"
          placeholder="Filter genes or genomic entities"
          autoComplete="off"
          onChange={onChange}
          className="flex-grow border py-2 px-3"
        />
        <span className="p-2">
          {entities.length} genes and genomic entities
        </span>
      </div>
      <div>
        Show{" "}
        <>
          <label htmlFor="genes" className="mr-1">
            Genes
          </label>
          <input
            type="checkbox"
            name="genes"
            id="genes"
            defaultChecked
            onChange={onChange}
            className="mr-2"
          />

          <label htmlFor="strs" className="mr-1">
            STRs
          </label>
          <input
            type="checkbox"
            name="strs"
            id="strs"
            defaultChecked
            onChange={onChange}
            className="mr-2"
          />

          <label htmlFor="regions" className="mr-1">
            Regions
          </label>
          <input
            type="checkbox"
            name="regions"
            id="regions"
            defaultChecked
            onChange={onChange}
          />
        </>
      </div>
    </>
  );
}
