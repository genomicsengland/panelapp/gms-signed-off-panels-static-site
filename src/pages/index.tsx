//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";

import Layout from "../layouts/Layout";

export default function Homepage() {
  return (
    <Layout>
      <div className="mx-auto pt-6 w-1/2">
        <p className="mb-3 font-bold">
          Welcome to the NHS Genomic Medicine Service (GMS) Signed Off Panels
          Resource
        </p>

        <p className="mb-3">
          Welcome to the resource to view the signed off panels that relate to
          genomic tests listed in the{" "}
          <a
            href="https://www.england.nhs.uk/genomics/the-national-genomic-test-directory/"
            className="text-nhs-blue hover:underline"
          >
            NHS National Genomic Test Directory
          </a>
          .
        </p>

        <p className="mb-3">
          The panels in this resource contain the list of Green (diagnostic
          level of evidence) genes*, short tandem repeats (STRs) and regions
          (copy number variants/CNVs) that have been approved for use in the NHS
          in England. These panels are reviewed and updated through an annual
          and quarterly{" "}
          <a
            href="https://www.england.nhs.uk/genomics/the-national-genomic-test-directory/"
            className="text-nhs-blue hover:underline"
          >
            evaluation process
          </a>{" "}
          supported by expert Test Evaluation Working Groups and overseen by the
          NHS England Genomics Clinical Reference Group.
        </p>

        <p className="mb-3 italic">
          * please note that some genes, in particular non-coding, mitochondrial
          and those located in difficult to sequence genomic regions, may not be
          accurately analysed via the GMS WGS service for rare and inherited
          diseases due to limitations of the WGS analysis pipeline or WGS
          technology. These tests are outside the scope of Genomics England's{" "}
          <a
            href="https://www.ukas.com/"
            className="text-nhs-blue hover:underline"
          >
            UKAS ISO 15189 accreditation
          </a>
          . For full details on GMS WGS limitations, please refer to the Rare
          Disease Analysis User Guide available in NHS Futures.
        </p>

        <p className="mb-3 font-bold">Searching</p>

        <p className="mb-3">
          Panels can be searched for by panel name or R code (e.g. R27) of the
          relevant clinical indication as shown in the National Genomic Test
          Directory. Search by gene or entity will show all the NHS GMS
          signed-off panels that the gene or entity is present on. In this
          release of the resource, some panels are listed which can only be
          requested as part of larger "superpanels". These component panels are
          flagged with a banner to indicate they cannot be requested directly as
          a standalone panel. In future versions of this site, only panels that
          can be requested directly as part of the clinical indications outlined
          in the National Genomic Test Directory will be displayed.
        </p>

        <p className="mb-3 font-bold">Leaving reviews for gene panels</p>

        <p className="mb-3">
          The main Genomics England{" "}
          <a
            href="https://panelapp.genomicsengland.co.uk/"
            className="text-nhs-blue hover:underline"
          >
            PanelApp
          </a>{" "}
          knowledgebase shows panels that have been updated by Genomics England
          curators and the content for these may differ to the panels on the GMS
          panels site.
        </p>

        <p className="mb-3">
          The PanelApp knowledgebase also lists entities on panels which are not
          yet rated Green (diagnostic level of evidence) but have lower levels
          of evidence for disease association (Amber and Red genes). The Red and
          Amber genes are not displayed on this GMS panels site.
        </p>

        <p className="mb-3">
          If you wish to add a review or suggest additional genes or entities
          for a panel, please do this in the main Genomics England PanelApp
          knowledgebase. Any changes to the content of the GMS panels will need
          to be evaluated through the genomic test evaluation process and will
          only be added to GMS panels following approval through this NHS
          England &amp; NHS Improvement process.
        </p>

        <p className="mb-3 font-bold">
          Questions about the National Genomic Test Directory
        </p>

        <p className="mb-10">
          For queries about tests within the Test Directory please refer to the
          following{" "}
          {
            <a
              href="https://www.england.nhs.uk/genomics/the-national-genomic-test-directory/"
              className="text-nhs-blue hover:underline"
            >
              page
            </a>
          }
          .
        </p>
      </div>
    </Layout>
  );
}
