//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";
import { graphql, Link } from "gatsby";
import Layout from "../layouts/Layout";

interface NotFoundProps {
  data?: {
    site: {
      siteMetadata: {
        panelappHostname: string;
      };
    };
  };
}

export default function NotFoundPage(props: NotFoundProps) {
  const panelappHostname = props.data?.site.siteMetadata?.panelappHostname;
  return (
    <Layout>
      <div className="mx-auto mt-24 w-1/2">
        <p className="mb-3 font-bold">Page Not Found</p>

        <p className="mb-3">
          The gene/entity, panel, or panel version you were looking for is not
          available on the GMS panels resource. Please retry your search via the{" "}
          <Link to={`/entities`} className="text-nhs-blue hover:underline">
            Genes and Entities search page
          </Link>{" "}
          or{" "}
          <Link to={`/panels`} className="text-nhs-blue hover:underline">
            Panels search page
          </Link>
          .
        </p>
        <p className="mb-3">
          You may wish to look at the main{" "}
          <a
            href={`https://${panelappHostname}`}
            className="text-nhs-blue hover:underline"
          >
            Genomics England PanelApp knowledgebase
          </a>{" "}
          to search across panels that are not restricted to GMS signed off
          panels.
        </p>
        <p className="mb-3">
          If you have further questions please contact the{" "}
          <a
            href="https://jiraservicedesk.extge.co.uk/plugins/servlet/desk/site/global"
            className="text-nhs-blue hover:underline"
          >
            Genomics England service desk
          </a>
        </p>
      </div>
    </Layout>
  );
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        panelappHostname
      }
    }
  }
`;
