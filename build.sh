#!/bin/sh

set -e

# Do not change this as monitoring/alerting depends on it:
at_exit () {
  rc=$?
  set +x
  echo "gatsby and sync: $(test "$rc" = 0 && echo "success" || echo "fail ($rc)")"
}
trap at_exit EXIT

set -x

npx gatsby info

rm -rf public
npx gatsby --no-color build

./aws_sync.js public "$S3_BUCKET"
