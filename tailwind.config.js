module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  content: [
    "./src/**/*.js",
    "./src/**/*.jsx",
    "./src/**/*.ts",
    "./src/**/*.tsx",
  ],
  theme: {
    extend: {
      colors: {
        "nhs-blue": "#005eb8",
        "nhs-green": "#009639",
        "nhs-orange": "#ed8b00",
        "nhs-red": "#d5281b",
        "nhs-warm-yellow": "#ffb81C",
        "nhs-pale-grey": "#f0f4f5",
        "nhs-mid-grey": "#d8dde0",
        "nhs-dark-green": "#006747",
        "nhs-dark-grey": "#aeb7bd",
        "nhs-dark-red": "#8a1538",
      },
    },
  },
  variants: {},
  plugins: [],
};
