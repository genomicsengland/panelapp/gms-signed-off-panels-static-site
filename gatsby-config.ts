/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

import type { GatsbyConfig } from "gatsby";

const {
  PANELAPP_HOSTNAME = "panelapp.genomicsengland.co.uk",
  GMS_PANEL_NOTICE_RD = "",
  GMS_PANEL_NOTICE_CANCER = "",
} = process.env;

const config: GatsbyConfig = {
  siteMetadata: {
    panelappHostname: PANELAPP_HOSTNAME,
    gmsPanelNoticeRd: GMS_PANEL_NOTICE_RD.split(",").map(id =>
      Number.parseInt(id, 10),
    ),
    gmsPanelNoticeCancer: GMS_PANEL_NOTICE_CANCER.split(",").map(id =>
      Number.parseInt(id, 10),
    ),
  },
  plugins: ["gatsby-plugin-postcss"],
  pathPrefix: "/panelapp/gms-signed-off-panels-static-site",
  trailingSlash: "ignore",
};

export default config;
