# checkov:skip=CKV_DOCKER_2: not a permanently running service (healthcheck)
# checkov:skip=CKV_DOCKER_7: false positive (non-latest tag)
ARG node_version=node:20.17.0-alpine3.19

FROM ${node_version} AS build

RUN apk upgrade --no-cache && \
    apk add --no-cache python3 gcc g++ make

RUN mkdir -p /app
COPY ./src /app/src
COPY ./static /app/static
COPY \
    ./aws_sync.js \
    ./build.sh \
    ./gatsby-browser.js \
    ./gatsby-config.ts \
    ./gatsby-node.ts \
    ./loadershim.js \
    ./package-lock.json \
    ./package.json \
    ./postcss.config.js \
    ./tailwind.config.js \
    ./tsconfig.json \
    ./utils.ts \
    /app/

RUN chmod +x /app/aws_sync.js /app/build.sh

RUN cd /app && npm ci --omit=dev


FROM ${node_version} AS final

RUN apk upgrade --no-cache && \
    apk add --no-cache aws-cli && \
    adduser -D -h /app appuser

COPY --from=build --chown=appuser /app /app

WORKDIR /app
ENV GATSBY_TELEMETRY_DISABLED=1
USER appuser
ENTRYPOINT ["/app/build.sh"]
