#!/usr/bin/env node

const { join } = require("path");
const { execFileSync } = require("child_process");
const {
  existsSync,
  mkdtempSync,
  readFileSync,
  readdirSync,
  rmSync,
  statSync,
  utimesSync,
  realpathSync,
} = require("fs");
const { createHash } = require("crypto");
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");

const VERSION_FILENAME = "version.json";

const region = process.env.AWS_REGION;
const endpoint = process.env.AWS_ENDPOINT_URL || undefined;
const aws_endpoint_option = endpoint ? [`--endpoint-url=${endpoint}`] : [];
const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
const app_version = process.env.APP_VERSION || "unknown";
const panelapp_host = process.env.PANELAPP_HOSTNAME || "unknown";
const upload_dir = realpathSync(process.argv[2]);
const download_dir = realpathSync(mkdtempSync("aws_sync-"));
const bucket = process.argv[3];
const loglevel = process.env.LOG_LEVEL
  ? process.env.LOG_LEVEL.toLowerCase()
  : "info";

let changed = false;

process.on("exit", () => rmSync(download_dir, { recursive: true }));

const debug = function (message) {
  if (loglevel === "debug") {
    console.log(message);
  }
};

const info = function (message) {
  console.log(message);
};

const hash = function (path) {
  return createHash("md5").update(readFileSync(path)).digest("hex");
};

const data_change_date = function () {
  const version_path = join(download_dir, VERSION_FILENAME);
  if (existsSync(version_path)) {
    return JSON.parse(readFileSync(version_path, "utf-8")).data_changed;
  } else {
    return new Date(0).toISOString();
  }
};

// TODO: use https://www.npmjs.com/package/s3-sync-client instead of aws s3 sync?
const aws_up_sync = function (args) {
  // prettier-ignore
  execFileSync(
    "aws",
    [
      ...aws_endpoint_option,
      "s3",
      "--region", region,
      "sync", upload_dir, `s3://${bucket}`,
      "--delete",
      "--sse", "AES256",
      ...args,
    ],
    {
      stdio: loglevel === "debug" ? "inherit" : "ignore"
    }
  );
};

const aws_down_sync = function () {
  execFileSync(
    "aws",
    [
      ...aws_endpoint_option,
      "s3",
      "sync",
      "--only-show-errors",
      `s3://${bucket}`,
      download_dir,
    ],
    {
      stdio: loglevel === "debug" ? "inherit" : "ignore",
    },
  );
};

const aws_upload_data = function (key, data) {
  const s3client = new S3Client({
    region,
    forcePathStyle: true,
    endpoint,
    credentials:
      accessKeyId && secretAccessKey
        ? { accessKeyId, secretAccessKey }
        : undefined,
  });

  const command = new PutObjectCommand({
    Bucket: bucket,
    Key: key,
    Body: data,
    ContentType: "application/json",
    ServerSideEncryption: "AES256",
    CacheControl: "public, max-age=0, must-revalidate",
  });

  (async () => {
    debug(await s3client.send(command));
  })().then();
};

const iterate_over_files = function (dir, action) {
  // recursive option requires node 20
  const files = readdirSync(dir);
  for (const file of files) {
    const path = join(dir, file);
    const fileStat = statSync(path);
    if (fileStat.isDirectory()) {
      iterate_over_files(path, action);
    } else {
      action(path);
    }
  }
};
const main = function () {
  aws_down_sync();

  iterate_over_files(download_dir, download_path => {
    const path = download_path.replace(`${download_dir}/`, "");
    const upload_path = download_path.replace(download_dir, upload_dir);
    if (path === VERSION_FILENAME) {
      debug(`present but ignored: ${path}`);
    } else if (!existsSync(upload_path)) {
      info(`not in upload: ${path}`);
      changed = true;
    } else if (hash(upload_path) === hash(download_path)) {
      utimesSync(upload_path, 0, 0);
      debug(`no change: ${path}`);
    } else {
      info(`content changed: ${path}`);
      changed = true;
    }
  });
  iterate_over_files(upload_dir, upload_path => {
    const download_path = upload_path.replace(upload_dir, download_dir);
    const path = upload_path.replace(upload_dir, "");
    if (!existsSync(download_path)) {
      info(`not in download: ${path}`);
      changed = true;
    }
  });

  if (changed) {
    aws_up_sync(
      // prettier-ignore
      [
        "--exclude", "*",
        "--include", "*.html",
        "--include", "page-data/*",
        "--include", "*.css",
        "--cache-control", "public, max-age=0, must-revalidate",
      ],
    );
    aws_up_sync(
      // prettier-ignore
      [
        "--exclude", "*.html",
        "--exclude", "page-data/*",
        "--exclude", "*.css",
        "--cache-control", "public, max-age=31536000, immutable",
      ],
    );
  } else {
    info("no changes");
  }

  const now = new Date().toISOString();
  const version = {
    application_version: app_version,
    panelapp_host: panelapp_host.replace(/[.].*/, ""),
    last_data_sync: now,
    data_changed: changed ? now : data_change_date(),
  };
  aws_upload_data(VERSION_FILENAME, JSON.stringify(version));
  info(version);
};

main();
